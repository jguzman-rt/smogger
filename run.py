#!/usr/bin/python3.5
import logging
import os
import configparser
from logging.config import dictConfig
from importlib import import_module

import pika
from main.consume import Consume

def get_logger(LOGGING):
    # Create log dir if missing.
    logpath = 'log'
    if not os.path.exists(logpath):
        os.makedirs(logpath)

    dictConfig(LOGGING)
    return logging.getLogger(__name__)


env = os.environ.get('SMOGGER_ENV', 'dev')
config = import_module('config.%s' % env)

logger = get_logger(config.LOGGING)
logger.info('Running in [%s] environment...' % env.upper())

connection_params = pika.ConnectionParameters(
    host=config.host, virtual_host=config.vhost)
connection = pika.BlockingConnection(connection_params)

callback = Consume(config).do
channel = connection.channel()
channel.basic_consume(callback, queue=config.queue_name)

logger.info('Waiting for messages...')
channel.start_consuming()
