#!/usr/bin/python3.5
import configparser
import os
import sys
import json
from importlib import import_module

env = os.environ.get('SMOGGER_ENV', 'dev')
config = import_module('config.%s' % env)

class_ = import_module('main.storage.%s' % config.store)
store = class_.Store()

# TODO: Allow searching by other params.
# TODO: Clean message_id.
message_id = sys.argv[1]
message = store.search_message(message_id)

print(json.dumps(message, indent=4, separators=(',', ': ')))
