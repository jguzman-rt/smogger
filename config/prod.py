# Import common configs.
from . import *

vhost = 'smpp'
host = '172.50.1.124'
queue_name = 'logstash_cb'
store = 'elasticsearch'

dequeue_messages = True  # remove processed messages

LOGGING = {
    'version': 1,
    'root': {
        'level': 'INFO',
        'handlers': ['file_error', 'file_info']
    },
    'filters': {
        'maxlevel_warning': {
            '()': MaxLevelFilter,
            'maxlevel': logging.WARNING
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'file_error': {
            'level': 'ERROR',
            'formatter': 'verbose',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'log/smogger-error.log',
            'maxBytes': 1024*1024*10, # 10MB
            'backupCount': 5,
        },
        'file_info': {
            'level': 'INFO',
            'filters': ['maxlevel_warning'],
            'formatter': 'verbose',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'log/smogger-info.log',
            'maxBytes': 1024*1024*10, # 10MB
            'backupCount': 5,
        },
    }
}
