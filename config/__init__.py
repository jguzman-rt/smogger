import logging

class MaxLevelFilter(logging.Filter):
    def __init__(self, maxlevel=None):
        self.maxlevel = maxlevel

    def filter(self, record):
        return record.levelno <= self.maxlevel

tracked_logs = {
    'fid': {
        'smpp.mt.pdu': 'msgid',
        'smpp.mt.invalid': '', # No fid key if rejected.
        'smpp.dr.pdu': 'mt-msgid',
        'pdu-to-wdoc.pdu.processed': 'msgid',
        'pdu-to-wdoc-expired-gc.debug': 'msgid',
        'mtdr.enqueued': 'mt_msgid'},
    'message_id': {
        'pdu-to-wdoc.wdoc': 'message_id',
        'traffic.eggrts.deferred': 'wdoc_message_id',
        'traffic.eggrts.confirmed': 'wdoc_message_id',
        'traffic.voyager.deferred': 'wdoc_message_id',
        'wdoc-to-mt-eggrts.wdoc.received.debug': 'message_id',
        'wdoc-to-mt-voyager.wdoc.received.debug': 'message_id'}}

tracked_keys = {
    'smpp.mt.pdu': {
        'fid': 'msgid',
        'datetime_received': 'server-unixtime',
        'from': 'pdu-src_addr',
        'to': 'pdu-dest_addr',
        'billing_account': 'pdu-bill_to',
        'username': 'smpp-username'},
    'smpp.mt.invalid': {
        'username': 'smpp-username',
        'datetime_received': 'server-unixtime',
        'error': 'error',
        'pdu_rejected_on_host': 'host',},
    'smpp.dr.pdu': {
        'datetime_dlr_received': 'server-unixtime',},
    'pdu-to-wdoc.pdu.processed': {
        'usagetype': 'usagetype',
        'datetime_processed': 'unixtime',
        'dlr_requested': 'dr_requested',
        'failure_dlr_requested': 'dr_requested_status_failure',
        'success_dlr_requested': 'dr_requested_status_success',
        'is_multipart': 'udhi'},
    'pdu-to-wdoc-expired-gc.debug': {
        'datetime_expired': 'gc_processed_time'},
    'mtdr.enqueued': {
        'datetime_dlr_generated': 'processed_unix_time',
        'dlr_generated_on_host': 'host',
        'dlr_added_to_queue': 'queue',
        'dlr_status_code': 'mtdr_code',
        'dlr_success': 'mtdr_success'},
    'pdu-to-wdoc.wdoc': {
        'pdu_processed_on_host': 'host',
        'datetime_wdoc_completed': 'processed_unix_time',
        'wdoc_completed_on_host': 'host',
        'message_id': 'message_id',
        'fid_list': 'body_parts_ids'}, # TODO: Should be array.
    'traffic.eggrts.deferred': {
        'datetime_deferred': 'processed_unix_time',
        'deferred_on_host': 'host',
        'body_length': 'body_length',
        'body_part_count': 'body_part_count',
        'deferred_status': 'deferred_status',
        'deferred_success': 'deferred_success',
        'transaction_id': 'req_rcvd_transid'},
    'traffic.eggrts.confirmed': {
        'datetime_confirmed': 'processed_unix_time',
        'confirmed_on_host': 'host',
        'confirmed_success': 'confirmed_success',},
    'traffic.voyager.deferred': {
        'datetime_deferred': 'processed_unix_time',
        'deferred_on_host': 'host',
        'body_length': 'body_length',
        'body_part_count': 'body_parts_count',
        'confirmed_success': 'deferred_success',
        'transaction_id': 'req_rcvd_transid'},
    'wdoc-to-mt-eggrts.wdoc.received.debug': {
        'body': 'body',},
    'wdoc-to-mt-voyager.wdoc.received.debug': {
        'body': 'body',}}
