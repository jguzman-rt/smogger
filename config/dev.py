# Import common configs.
from . import *

vhost = '/'
host = 'localhost'
queue_name = 'logstash_cb'
store = 'file'

dequeue_messages = False  # dev does not actually remove messages from queue

LOGGING = {
    'version': 1,
    'root': {
        'level': 'DEBUG',
        'handlers': ['file_error', 'file_info', 'console']
    },
    'filters': {
        'maxlevel_warning': {
            '()': MaxLevelFilter,
            'maxlevel': logging.WARNING
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'file_error': {
            'level': 'ERROR',
            'formatter': 'verbose',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'log/smogger-error.log',
            'maxBytes': 1024*1024*10, # 10MB
            'backupCount': 5,
        },
        'file_info': {
            'level': 'INFO',
            'filters': ['maxlevel_warning'],
            'formatter': 'verbose',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'log/smogger-info.log',
            'maxBytes': 1024*1024*10, # 10MB
            'backupCount': 5,
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    }
}
