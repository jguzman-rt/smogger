import unittest
from main.consume import Consume
from importlib import import_module


class MockPikaChannel:
    is_acked = False
    is_rejected = False

    def basic_ack(self, delivery_tag):
        self.is_acked = True

    def basic_reject(self, delivery_tag, requeue=True):
        self.is_rejected = True

class MockPikaMethod:
    delivery_tag = 'mock'

class TestConsume(unittest.TestCase):
    def setUp(self):
        test_config = import_module('config.test')
        self.consumer = Consume(test_config)
        self.ch = MockPikaChannel()
        self.method = MockPikaMethod()
        
    def test_can_accept_mt_invalid_content(self):
        raw_content=b'{"log": "smpp.mt.invalid", "error": "MockError"}'
        self.consumer.do(self.ch, self.method, {}, body=raw_content)
        self.assertEqual(self.consumer.store.last_message['error'], 'MockError')
        self.assertTrue(self.ch.is_acked)

    def test_can_accept_mt_pdu_content(self):
        raw_content=b'{"log": "smpp.mt.pdu", "msgid": "mockfid"}'
        self.consumer.do(self.ch, self.method, {}, body=raw_content)
        self.assertEqual(self.consumer.store.last_message_id, 'mockfid')
        self.assertTrue(self.ch.is_acked)

    def test_cannot_accept_fid_type_without_fid(self):
        raw_content=b'{"log": "smpp.mt.pdu"}'
        self.consumer.do(self.ch, self.method, {}, body=raw_content)
        self.assertEqual(self.consumer.store.last_message, None)
        self.assertFalse(self.ch.is_acked)
        self.assertTrue(self.ch.is_rejected)

    def test_can_accept_traffi_log(self):
        raw_content=b'{"log": "traffic.eggrts.confirmed", "wdoc_message_id":"mockid"}'
        self.consumer.do(self.ch, self.method, {}, body=raw_content)
        self.assertEqual(self.consumer.store.last_message_id, 'mockid')
        self.assertTrue(self.ch.is_acked)

    def test_cannot_accept_msgid_type_without_msgid(self):
        raw_content=b'{"log": "traffic.eggrts.confirmed"}'
        self.consumer.do(self.ch, self.method, {}, body=raw_content)
        self.assertEqual(self.consumer.store.last_message, None)
        self.assertFalse(self.ch.is_acked)
        self.assertTrue(self.ch.is_rejected)
