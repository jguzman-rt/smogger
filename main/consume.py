import json
import os
import logging
from importlib import import_module

import arrow

class Consume(object):
    def __init__(self, config):
        # Dynamic import of Backend Storage class to use.
        class_ = import_module('main.storage.%s' % config.store)
        self.store = class_.Store()

        self.tracked_logs = config.tracked_logs
        self.tracked_keys = config.tracked_keys
        self.dequeue_messages = config.dequeue_messages

        self.logger = None

    def _ack(self, ch, method):
        if self.dequeue_messages:
            ch.basic_ack(delivery_tag=method.delivery_tag)
            return True
        return False

    def _reject(self, ch, method, requeue):
        if self.dequeue_messages:
             ch.basic_reject(delivery_tag=method.delivery_tag, requeue=requeue)
             return True
        return False

    def _find_type(self, logname):
        for id_ in self.tracked_logs.keys():
            for logname_ in self.tracked_logs[id_]:
                if logname == logname_:
                    return id_
        return None

    def _format(self, message, logname):
        formatted = {}
        tracked_keys = self.tracked_keys[logname].items()
        for key, val in tracked_keys:
            formatted[key] = message.get(val, None)
        return formatted

    def _get_logger(self):
        if self.logger is None:
            self.logger = logging.getLogger(__name__)
        return self.logger


    def do(self, ch, method, properties, body):
        content = json.loads(body.decode('utf-8'))

        logname = content['log']
        type_ = self._find_type(logname)
        if type_ is None:
            logger = self._get_logger()
            logger.debug('Ignoring log: %s' % logname)

            return self._ack(ch, method)

        # Get message_id/fid in log.
        # Use epoch time for logs from smpp reject logs (e.g., smpp.mt.invalid).
        message_id_key = self.tracked_logs[type_][logname]
        message_id = content.get(message_id_key, None)\
            if message_id_key\
            else int(arrow.utcnow().float_timestamp*1000)

        # check here if content is invalid by checking message_id
        if message_id is None:
            logger = self._get_logger()
            logger.error('Rejecting log from: %s content: %s' % (logname, json.dumps(content)))
            return self._reject(ch, method, False)

        new_message = self._format(content, logname)
        self.store.write_message(new_message, message_id, type_)

        self._ack(ch, method)
