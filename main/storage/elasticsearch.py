import json
import logging
import os

import arrow
from elasticsearch import Elasticsearch, NotFoundError
from . import BaseStorage

class Store(BaseStorage):
    def __init__(self):
        # No need to put in config; it's specific in this module.
        es_connect = [{'host': 'localhost', 'port': 9200}]
        if os.environ.get('SMOGGER_ENV', 'dev') == 'prod':
            es_connect = [{'host': '172.50.1.163', 'port': 9200}]

        self.es = Elasticsearch(es_connect)
        self.logger = logging.getLogger(__name__)

    def _get_index(self, type_, ymd='*'):
        return 'smogger_%s_%s' % (type_, ymd)

    def _get_type(self):
        # TODO: Should this change?
        # This supposed to group events based on level (e.g. info, error)
        return 'info'


    def get_message(self, message_id, type_):
        try:
            res = self.es.get(
                index=self._get_index(type_),
                doc_type=self._get_type(),
                id=message_id)
            message = res['_source']

            self.logger.debug('%s: %s' % (type_, message_id))
            return message
        except NotFoundError:
            return {}


    def write_message(self, new_message, message_id, type_):
        message = self.get_message(message_id, type_)

        if message:
            # Hash merge, only in >Python3.5.
            new_message = {**new_message, **message}

        # Add processed timestamp.
        now = arrow.utcnow().to('Asia/Manila')
        new_message['smogtime'] = now.isoformat()

        self.es.index(
            index=self._get_index(type_, now.format('YYYY.MM.DD')),
            doc_type=self._get_type(),
            id=message_id,
            body=json.dumps(new_message))

        return new_message


    def search_message(self, message_id):
        message = self.get_message(message_id, 'message_id')

        if 'fid_list' in message.keys():
            pdus = []
            fids = message['fid_list'].split(',')

            for _ in fids:
                pdus.append(self.get_message(_.strip(), 'fid'))

            message['pdus'] = pdus

        return message
