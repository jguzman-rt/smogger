import os
import json
import logging

from . import BaseStorage

class Store(BaseStorage):
    last_message = None
    last_message_id = None
    last_type = None

    def __init__(self):
        pass

    def get_message(self, message_id, type_):
        return None


    def write_message(self, new_message, message_id, type_):
        self.last_message = new_message
        self.last_message_id = message_id
        self.last_type = type_

        return new_message

