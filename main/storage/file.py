import os
import json
import logging

from . import BaseStorage

class Store(BaseStorage):
    def __init__(self):
        store_path = {
            'message_id': 'filestore/message_id',
            'fid': 'filestore/fid'}

        # Create filestore dirs if missing.
        for type_, path in store_path.items():
            if not os.path.exists(path):
                os.makedirs(path)

        self.store_path = store_path

    def _get_file(self, type_, message_id):
        return '%s/%s.json' % (self.store_path[type_], message_id)

    def _read_file(self, file):
        if not os.path.isfile(file):
            return {}

        with open(file) as handle:
            content = json.loads(handle.read())

        return content


    def get_message(self, message_id, type_):
        file = self._get_file(type_, message_id)
        message = self._read_file(file)

        if message:
            logger = logging.getLogger(__name__)
            logger.debug('%s: %s' % (type_, message_id))

        return message


    def write_message(self, new_message, message_id, type_):
        message = self.get_message(message_id, type_)

        # Hash merge, only in >Python3.5.
        new_message = {**new_message, **message}

        file = self._get_file(type_, message_id)
        with open(file, 'w+') as handle:
            handle.write(json.dumps(new_message))

        return new_message


    def search_message(self, message_id):
        file = self._get_file('message_id', message_id)
        message = self._read_file(file)

        if 'fid_list' in message.keys():
            pdus = []
            fids = message['fid_list'].split(',')

            for _ in fids:
                fid_file = self._get_file('fid', _.strip())
                pdus.append(self._read_file(fid_file))

            message['pdus'] = pdus

        return message
