from abc import ABCMeta, abstractmethod

class BaseStorage(metaclass=ABCMeta):
    @abstractmethod
    def get_message(self):
        pass

    @abstractmethod
    def write_message(self):
        pass
